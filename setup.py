#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="python-kinesis-handler",
    version='1.0.1',
    description="Logging handler to send logs to your Kinesis Stream",
    keywords="logging handler kinesis",
    author="twash",
    author_email="troy.washburn@7-11.com",
    url="https://gitlab.com/7-Eleven/python-kinesis-handler/",
    license="Apache License 2",
    packages=find_packages(),
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 2.7'
    ]
)