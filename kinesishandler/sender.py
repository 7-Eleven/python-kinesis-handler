# This class is responsible for handling all asynchronous communication
import sys
import json
import boto3
import random
import string
from botocore.credentials import RefreshableCredentials
from botocore.session import get_session

from time import sleep
from datetime import datetime
from threading import Thread, enumerate

from .logger import get_logger

if sys.version[0] == '2':
    import Queue as queue
else:
    import queue as queue


# Each PutRecords request can support up to 500 records. Each record in the
# request can be as large as 1 MB, up to a limit of 5 MB for the entire request,
# including partition keys. Each shard can support writes up to 1,000 records per
# second, up to a maximum data write total of 1 MB per second.

MAX_BULK_SIZE_IN_BYTES = 1 * 1024 * 1024  # 1 MB


def backup_logs(logs, logger):
    timestamp = datetime.now().strftime('%d%m%Y-%H%M%S')
    logger.info(
        'Backing up your logs to log-failures-%s.txt', timestamp)
    with open('log-failures-{}.txt'.format(timestamp), 'a') as f:
        f.writelines('\n'.join(logs))

class LogSender:
    def __init__(self,
                 stream_name,
                 writerRoleArn,
                 region="us-east-1",
                 logs_drain_timeout=5,
                 sleep_between_retries=2,
                 number_of_retries=4,
                 should_backup_to_disk=True,
                 debug=False):
        self.write_role_arn = writerRoleArn
        self.region = region
        self._initialize_kinesis_client()
        self.stream_name = stream_name
        self.logs_drain_timeout = logs_drain_timeout
        self.sleep_between_retries = sleep_between_retries
        self.number_of_retries = number_of_retries
        self.should_backup_to_disk = should_backup_to_disk
        self.logger = get_logger(debug)

        # Function to see if the main thread is alive
        self.is_main_thread_active = lambda: any(
            (i.name == 'MainThread') and i.is_alive() for i in enumerate())

        # Create a queue to hold logs
        self.queue = queue.Queue()
        self._initialize_sending_thread()

    
    def _refresh_creds(self):
        # Create session credentials by assuming a writerrole.
        sts_default_provider_chain = boto3.client('sts', region_name=self.region)
        params = {
            "RoleArn": self.write_role_arn,
            "RoleSessionName": "KinesisLogWriterSession",
            "DurationSeconds": 3600, #Need to check, if we want to make this configurable
        }
        sts_creds = sts_default_provider_chain.assume_role(**params).get("Credentials")
        metadata = {
            "access_key": sts_creds.get("AccessKeyId"),
            "secret_key": sts_creds.get("SecretAccessKey"),
            "token": sts_creds.get("SessionToken"),
            "expiry_time": sts_creds.get("Expiration").isoformat(),
        }
        return metadata

    def _initialize_kinesis_client(self):
        # Create a kinesis client using boto3 session only when writer_role_arn is present
        if self.write_role_arn is not None:
            session = get_session()
            # Create a botocore session, and use RefreshableCredentials from botocore to refresh the credentials, when the old credentials expires.
            session._credentials = RefreshableCredentials.create_from_metadata(
                        metadata=self._refresh_creds(),
                        refresh_using=self._refresh_creds,
                        method="sts-assume-role",
                    )
            session.set_config_variable("region", self.region)
            autorefresh_session = boto3.Session(botocore_session=session)
            self.kinesis = autorefresh_session.client("kinesis", region_name=self.region)
        else:
            self.kinesis = boto3.client("kinesis", region_name=self.region)


    def _initialize_sending_thread(self):
        self.sending_thread = Thread(target=self._drain_queue)
        self.sending_thread.daemon = False
        self.sending_thread.name = 'log-sending-thread'
        self.sending_thread.start()

    def append(self, logs_message):
        if not self.sending_thread.is_alive():
            self._initialize_sending_thread()

        # Queue lib is thread safe, no issue here
        self.queue.put(json.dumps(logs_message))

    def flush(self):
        self._flush_queue()

    def _drain_queue(self):
        last_try = False

        while not last_try:
            # If main is exited, we should run one last time and try to remove
            # all logs
            if not self.is_main_thread_active():
                self.logger.debug(
                    'Identified quit of main thread, sending logs one '
                    'last time')
                last_try = True

            try:
                self._flush_queue()
            except Exception as e:
                self.logger.debug(
                    'Unexpected exception while draining queue to Kinesis, '
                    'swallowing. Exception: %s', e)

            if not last_try:
                sleep(self.logs_drain_timeout)

    def _prepare(self, records):
        def fmt(record):
            return { 'PartitionKey' : ''.join(random.choices(string.ascii_uppercase + string.digits, k=10)), 'Data' : record }

        return [fmt(record) for record in records]

    def _flush_queue(self):
        # Sending logs until queue is empty
        while not self.queue.empty():
            logs_list = self._get_messages_up_to_max_allowed_size()
            self.logger.debug(
                'Starting to drain %s logs to Kinesis', len(logs_list))

            # Configurable from the outside
            sleep_between_retries = self.sleep_between_retries
            should_backup_to_disk = self.should_backup_to_disk

            for current_try in range(self.number_of_retries):
                should_retry = False
                try:
                    data = self._prepare(logs_list)
                    self.kinesis.put_records(Records=data, StreamName=self.stream_name)
                    self.logger.debug(
                        'Successfully sent bulk of %s logs to '
                        'Kinesis!', len(logs_list))
                    should_backup_to_disk = False
                    break
                except Exception as e:
                    self.logger.error(
                        'Got exception while sending logs to Kinesis, '
                        'Try (%s/%s). Message: %s',
                        current_try + 1, self.number_of_retries, e)
                    should_retry = True

                if should_retry:
                    sleep(sleep_between_retries)
                    sleep_between_retries *= 2

            if should_backup_to_disk:
                # Write to file
                self.logger.info(
                    'Could not send logs to Kinesis after %s tries, '
                    'backing up to local file system', self.number_of_retries)
                backup_logs(logs_list, self.logger)

    def _get_messages_up_to_max_allowed_size(self):
        logs_list = []
        current_size = 0
        while not self.queue.empty():
            current_log = self.queue.get()
            current_size += sys.getsizeof(current_log)
            logs_list.append(current_log)
            if current_size >= MAX_BULK_SIZE_IN_BYTES:
                break
        return logs_list
