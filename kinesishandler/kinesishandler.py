import os
import sys
import json
import logging
import datetime
import traceback
import logging.handlers

from .sender import LogSender
from .exceptions import LogException


class KinesisHandler(logging.handlers.BufferingHandler):

    def __init__(self,
                 stream_name,
                 writer_role_arn=None,
                 region="us-east-1",
                 logs_drain_timeout=5,
                 sleep_between_retries=2,
                 number_of_retries=4,
                 should_backup_to_disk=True,
                 debug=False,
                 log_type="python"):

        if not stream_name:
            raise LogException('Kinesis stream name must be provided')

        self.log_type = log_type

        self.log_sender = LogSender(
            stream_name=self.get_env(stream_name),
            writerRoleArn= self.get_env(writer_role_arn),
            region=self.get_env(region),
            logs_drain_timeout=logs_drain_timeout,
            sleep_between_retries=sleep_between_retries,
            number_of_retries=number_of_retries,
            should_backup_to_disk=should_backup_to_disk,
            debug=debug)
        logging.Handler.__init__(self)
    
    def get_env(self, value):
        if sys.version_info < (3, 0):
            # long and basestring don't exist in py3 so, NOQA
            var_type = (basestring)  # NOQA
        else:
            var_type = (str)

        if isinstance(value, var_type):
            return os.path.expandvars(value)
        else:
            return value
    
    def extra_fields(self, message):

        not_allowed_keys = (
            'args', 'asctime', 'created', 'exc_info', 'stack_info', 'exc_text',
            'filename', 'funcName', 'levelname', 'levelno', 'lineno', 'module',
            'msecs', 'message', 'msg', 'name', 'pathname', 'process',
            'processName', 'relativeCreated', 'thread', 'threadName')

        if sys.version_info < (3, 0):
            # long and basestring don't exist in py3 so, NOQA
            var_type = (basestring, bool, dict, float,  # NOQA
                        int, long, list, type(None))  # NOQA
        else:
            var_type = (str, bool, dict, float, int, list, type(None))

        extra_fields = {}

        for key, value in message.__dict__.items():
            if key not in not_allowed_keys:
                if isinstance(value, var_type):
                    extra_fields[key] = self.get_env(value)
                else:
                    extra_fields[key] = self.get_env(repr(value))

        return extra_fields

    def flush(self):
        self.log_sender.flush()

    def format(self, record):
        message = os.path.expandvars(super(KinesisHandler, self).format(record))
        try:
            return json.loads(message)
        except (TypeError, ValueError):
            return message

    def format_exception(self, exc_info):
        return '\n'.join(traceback.format_exception(*exc_info))

    def format_message(self, message):
        now = datetime.datetime.utcnow()
        timestamp = now.strftime('%Y-%m-%dT%H:%M:%S') + \
            '.%03d' % (now.microsecond / 1000) + 'Z'

        return_json = {
            'logger': message.name,
            'line_number': message.lineno,
            'path_name': message.pathname,
            'log_level': message.levelname,
            'type': self.log_type,
            'message': message.getMessage(),
            '@timestamp': timestamp
        }

        if message.exc_info:
            return_json['exception'] = self.format_exception(message.exc_info)

            # We want to ignore default logging formatting on exceptions
            # As we handle those differently directly into exception field
            message.exc_info = None
            message.exc_text = None

        formatted_message = self.format(message)
        if isinstance(formatted_message, dict):
            return_json.update(formatted_message)
        else:
            return_json['message'] = formatted_message

        return_json.update(self.extra_fields(message))
        return return_json

    def emit(self, record):
        self.log_sender.append(self.format_message(record))
