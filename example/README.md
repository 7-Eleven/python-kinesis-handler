# Python Kinesis Handler Logging Example

This example shows how to use the [Python Logging facility](https://docs.python.org/library/logging.html) with the [Kinesis Handler](https://github.com/joekickass/python-kinesis-logger) to centralize your Python logs.

While this example prints logs both to the console & remotely to Kinesis, the end goal should just be to send remote logs to Kinesis and not to the console or a file.  This will have the best performance of minimizing I/O, as well as the benefit of not having to manage log rotations or disk space.

### High level steps

At a high level, here are the steps (review [Dockerfile](Dockerfile) for more specifics):
- Create/update your Python *requirements.txt* file to include the needed dependencies (review [requirements.txt](requirements.txt))
- Create/update your *logging.conf* file to include the *Kinesis* handler and desired properties (review [logging.conf](logging.conf))
- Package the necessary dependencies into your application & run

### Requirements

To run this example, you must:
- have the ability to clone this code repo
- have installed [Docker](https://www.docker.com/)
- have the ability for Docker to pull images from [Docker Hub](https://hub.docker.com/)
- have AWS credentials to be able to write to a Kinesis Stream
- have the ability to communicate to *kinesis.us-east-1.amazonaws.com* over secure *port 443*

### Build the example

```shell
$ docker build -t examples/python-logging-example:1 .
```

### Run the example

```shell
$ docker run -e 'AWS_ACCESS_KEY_ID=<access key id>' -e 'AWS_SECRET_ACCESS_KEY=<secret access key>' -e 'ENVIRONMENT=<environment>' -e 'APPLICATION=<app-name>' -e 'KINESIS_STREAM=<kinesis-stream-name>' examples/python-logging-example:1
```
