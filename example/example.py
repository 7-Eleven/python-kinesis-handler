import os
import yaml
import logging
import logging.config
import logging.handlers

# Global variables
initialized = False
cached = False
correlationId = None
logger = None

class MetaFilter(logging.Filter):
    def filter(self, record):
        global cached, correlationId

        record.team = "MyTeamName"
        record.cached = cached
        if (correlationId != None):
            record.correlationId = correlationId
        return True

def initialize():
    global initialized, cached, logger

    if initialized:
        # Record we've been cached (e.g. warm lambda)
        cached = True
        return

    # Mark as initialized (i.e. so we only do this once for each cold lambda)
    initialized = True

    # configure logging from file
    logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))

    # get my logger
    logger = logging.getLogger(__name__)

def flush():
    global logger

    for handler in logger.handlers:
        if isinstance(handler, logging.handlers.BufferingHandler):
            handler.flush()

    return True

def main():
    global logger, correlationId

    # Initialize, if needed
    initialize()

    # Standard messages
    logger.info("This is the my standard message.")

    # or with meta info
    logger.info("This is the log message!", extra={ "snakes": "delicious" })

    # Set correlation id for cross-module references (sort on correlation id to see all related events)
    correlationId = "my-correlation-id-1"

    # Warning & debug message
    logger.warning("My fake warning", extra={ "myname": "troy" })
    logger.debug("My debug message")

    # Set correlation id to another value to make sure it works
    correlationId = "my-correlation-id-2"

    # Mark message as "secure" for 7Logs app to route appropriately
    logger.info("My secure message", extra={ "secure": True })

    # Other log messages - shows default log levels
    logger.critical("My critical message")
    logger.error("My error message")
    logger.warning("My warning message")
    logger.info("My info message")
    logger.debug("My debug message")

def lambda_handler(event, context):
    # Call main method
    main()

    # Make sure to flush on each invocation end
    flush()

if __name__ == "__main__":
    # Call main method
    main()

    # Only at application exit (will auto-flush)
    logging.shutdown()
