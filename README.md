# Python Kinesis Handler

Inspired by the [Logz.io Python Handler](https://github.com/logzio/logzio-python-handler), this is a [Python logging](https://docs.python.org/library/logging.html) Kinesis Handler that sends logs in bulk to an [AWS Kinesis Stream](https://aws.amazon.com/kinesis/).

## Installation
```bash
pip install git+https://gitlab.com/7-Eleven/python-kinesis-handler#egg=python-kinesis-handler
```

## Python logging configuration
#### Config File (logging.conf)
```
version: 1
formatters:
  simple:
    format: '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
  metadata:
    format: '{"application": "${APPLICATION}", "environment": "${ENVIRONMENT}"}'
handlers:
  console:
    class: logging.StreamHandler
    formatter: simple
    stream: ext://sys.stdout
  kinesis:
    class: kinesishandler.KinesisHandler
    level: DEBUG
    formatter: metadata
    stream_name: ${KINESIS_STREAM}
    # writer_role_arn: ${WRITER_ROLE_ARN} ## This field is required while running the application on aws fargate/lambda functions and destination stream is in different aws account.    
    # region: us-east-1
    # logs_drain_timeout: 5
    # sleep_between_retries: 2
    # number_of_retries: 4
    # should_backup_to_disk: True
    # debug: False
    # log_type: python
loggers:
  __main__:
    level: INFO
    propagate: no
    handlers: [console, kinesis]
root:
  level: DEBUG
  handlers: [console]
```

Where:
 - **stream_name**: AWS Kinesis Stream name (*required*)
 - **writer_role_arn**: AWS cross account access role ARN.This field is required when you want to write logs to Kinesis stream in different AWS account(*defaults to None*). 
 - **region**: AWS Region where Kinesis Stream is located (*defaults to "us-east-1"*)
 - **logs_drain_timeout**: Time to sleep between draining attempts (*defaults to "5"*)
 - **sleep_between_retries**: Time to sleep between retries on failures (*defaults to "2", has a progressive backoff strategy*)
 - **number_of_retries**: Number of attempts to send to Kinesis (*defaults to "4"*)
 - **should_backup_to_disk**: Writes logs to disk after exhausting retries (*defaults to "True"*)
 - **debug**: Set to True, will print debug messages to stdout. (*defaults to "False"*)

#### Code Example
```python
import os
import yaml
import logging
import logging.config

# Say i have saved my configuration under ./logging.conf
logging.config.dictConfig(yaml.load(open('logging.conf', 'r')))
logger = logging.getLogger(__name__)

logger.info('Test log')
logger.warning('Warning')

try:
    1/0
except:
    logger.exception('Supporting exceptions too!')

logging.shutdown()
```

For a more complete example, you can reference the [example](example) directory.

#### Extra Fields
In case you need to dynamic metadata to your logger, other then the constant metadata from the formatter, you can use the "extra" parameter.
All key values in the dictionary passed in "extra" will be presented as new fields in the log you are sending.
Please note, that you cannot override default fields by the python logger (i.e. lineno, thread, etc..)
For example:

```
logger.info('Warning', extra={'extra_key':'extra_value'})
```

## Release Notes
- 1.0.0 - Initial release